# Pushover CLI

A command line interface for [Pushover](https://pushover.net/) notifications. This project is not written, maintained, or supported by Superblock (the creators of Pushover), and is not an official tool.

## Installation

```
curl -s https://bitbucket.org/gufertum/pushover-cli/raw/master/install.sh | sudo bash
```

## Usage

Run `pushover --help` for usage instructions.

To send a short ping or msg use `po Your Message goes here`. This command does not support options and just sends all arguments as msg and even works without arguments (sends a ping).

### Configuration

This option allows you to set a global user key and application API token. This can be overridden on a per-use basis by specifying the `--user` and `--token` options.

```
pushover --config
```

This generates a config file `$HOME/.pushover`.
This config file can also be provided as argument `--config-file /path/to/config` to switch configs or in case you run in a non-env setting (cron).

### Upgrade

This option will upgrade to the latest release version.

```
pushover --upgrade
```

# Contributors
* Aaron Fagan - [Github](https://github.com/aaronfagan), [Website](https://www.aaronfagan.ca/)
* Thomas Sch�dler [Bitbucket](https://bitbucket.org/gufertum/pushover-cli), [Website](https://lambda.li/)
