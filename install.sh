#!/bin/bash

echo -ne '\033[0;37m'
echo -ne "\nDownloading Pushover CLI... "
BRANCH="${1}"
if [ "${BRANCH}" == "dev" ]; then BRANCH="develop"; else BRANCH="master"; fi
git clone --branch "${BRANCH}" --depth 1 https://gufertum@bitbucket.org/gufertum/pushover-cli.git /tmp/pushover-cli
ls -1 /tmp/pushover-cli/pushover
cp /tmp/pushover-cli/pushover /usr/local/bin/pushover
cp /tmp/pushover-cli/po /usr/local/bin/po
chmod +x /usr/local/bin/pushover
chmod +x /usr/local/bin/po
echo -ne " done!"
VERSION=$(awk -F'"' '/^VERSION=/ {print $2}' /usr/local/bin/pushover)
echo -ne "\nPushover CLI (v${VERSION}) installed successfully! Run 'pushover --help' for usage instructions.\n\n"
echo -ne '\033[0m'
if [ "${2}" != "upgrade" ]; then rm -rf /tmp/pushover-cli; fi
